$(document).ready ->

  $('#exchange_date').datepicker
    format: 'dd-mm-yyyy',
    startDate: "01-01-2000",
    endDate: new Date,
    daysOfWeekDisabled: "0,6",
    startView: 2,
    maxViewMode: 2,
    todayBtn: true,
    autoclose: true