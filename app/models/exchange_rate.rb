class ExchangeRate < ActiveRecord::Base

  def self.convert(amount, datestring)
    begin
      date = Date.parse(datestring)
    rescue ArgumentError
      return "You didn't provide a valid date format."
    end
    return 'You cannot use a date in the future or before 01-01-2000' unless valid_date?(date)
    exchange_rate = find_by_date(date)
    amount_as_float = amount.gsub(',','.').to_f
    (amount_as_float / exchange_rate).round(4)
  end

  private

  def self.find_by_date(date)
    until exchange_rate = ExchangeRate.find_by(date: date)
      date -= 1.day
    end
    exchange_rate.rate
  end

  def self.valid_date?(date)
    date <= Date.current && date >= Date.new(2000,1,1)
  end

end
