class ExchangeRatesController < ApplicationController

  def index
  end

  def convert
    amount = params['dollar_amount']
    date = params['exchange_date']
    converted_amount = ExchangeRate.convert(amount, date)
    @conversion_data = {
      amount: amount,
      date: Date.parse(date),
      converted: converted_amount
    }

    respond_to { |format| format.html { render action: "index" } }
  end

end
