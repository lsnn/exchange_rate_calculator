FactoryGirl.define do
  
  factory :exchange_rate do
    date "01-01-2016"
    rate  "1.1234"
  end

end