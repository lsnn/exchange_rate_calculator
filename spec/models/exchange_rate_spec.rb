require 'rails_helper'

RSpec.describe ExchangeRate, type: :model do

  describe 'when converting a value' do
    let(:datestring) { '01-06-2016' }
    let(:rate) { 1.3456 }
    FactoryGirl.create :exchange_rate, rate: 1.1234, date: '01-01-2016'
    FactoryGirl.create :exchange_rate, rate: 1.3456, date: '01-06-2016'
    FactoryGirl.create :exchange_rate, rate: 1.5678, date: '01-03-2017'

    it 'uses the correct exchange rate for a specified date' do
      expect(ExchangeRate.find_by_date(Date.parse datestring)).to eq rate
    end

    it 'uses the previous available rate for missing dates' do
      expect(ExchangeRate.find_by_date(Date.parse '02-06-2016')).to eq rate
    end

    it 'gives an error when choosing a date further than today' do
      expect(ExchangeRate.convert('200', '10-02-2018')).to eq 'You cannot use a date in the future or before 01-01-2000'
    end

    it 'gives an error when choosing a date before 01-01-2000' do
      expect(ExchangeRate.convert('200', '10-02-1999')).to eq 'You cannot use a date in the future or before 01-01-2000'
    end

    it 'gives an error when giving an invalid date format' do
      expect(ExchangeRate.convert('200', '00-06-2016')).to eq "You didn't provide a valid date format."
    end

    it 'calculates the correct value in Euros for a specified date' do
      expect(ExchangeRate.convert('200', datestring)).to eq(148.6326)
    end

    it 'should treat comma"s as points' do
      expect(ExchangeRate.convert('3.5', datestring)).to eq(2.6011)
      expect(ExchangeRate.convert('3,5', datestring)).to eq(2.6011)
    end
    
  end

end