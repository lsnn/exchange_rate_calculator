# Exchange Rate Calculator

##### This simple app can convert an amount in Dollars to Euros with the correct exchange rate for any given date between 01-01-2000 and now. 

### Usage
It is not deployed to a live environment. There are two ways to use the app:

#### on the commandline
```sh
cd exchange_rate_calculator
rails c
ExchangeRate.convert('200', '10-02-2016')
# with '200' being the amount in dollars and the second value being any date between 01-01-2000 and today.
```

#### run a local server and use the interface
```sh
cd exchange_rate_calculator
bundle install
rake db:create
rake db:migrate
rails s
```
Then go to ```localhost:3000``` in your browser and you can use the app.
There are no exchange rates available for weekend days and bank holidays. The app will use the closest previous exchange rate.

## Development
### Details
- Ruby 2.3.3
- Rails 4.2.1
- Rspec
- sqlite3

There is a task to import all Dollar/Euro exchange rates between 01-01-2000 and today. This task is idempotent and won't store double data.
The task can be run from the commandline with
```
rake import:euro_dollar_exchange_rates
```