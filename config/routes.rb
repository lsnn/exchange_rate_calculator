Rails.application.routes.draw do

  root 'exchange_rates#index'

  post 'convert', to: 'exchange_rates#convert'

end
