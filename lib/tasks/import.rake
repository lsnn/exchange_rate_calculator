namespace :import do
  desc "Imports a csv file with exchange rates between Euros and Dollars sinds January 1st 2000."
  task euro_dollar_exchange_rates: :environment do

    require 'open-uri'
    require 'csv'
    url = "http://sdw.ecb.europa.eu/quickviewexport.do?SERIES_KEY=120.EXR.D.USD.EUR.SP00.A&type=csv"

    begin
      exchange_rates = open(url)
    rescue OpenURI::HTTPError => error
      response = error.io
      response.status
    end
    CSV.foreach(exchange_rates, return_headers: false) do |row|
      date = Date.parse(row.first) rescue nil
      if date
        # Exiting the loop when a date already exists, rates from the past can't change
        # value. Only new rates will be saved. Also exit when dates are before
        # January 1st 2000.
        break if ExchangeRate.find_by(date: date, rate: row.last)
        break if date < Date.new(2000,1,1)
        ExchangeRate.create(date: date, rate: row.last)
      end
    end
  end

end
