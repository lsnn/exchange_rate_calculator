class CreateExchangeRates < ActiveRecord::Migration
  def change
    create_table :exchange_rates do |t|
      t.date :date
      t.float :rate

      t.timestamps null: false
    end
  end
end
