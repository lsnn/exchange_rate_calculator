class AddIndexToExchangeRates < ActiveRecord::Migration
  def change
    add_index :exchange_rates, :date
  end
end
